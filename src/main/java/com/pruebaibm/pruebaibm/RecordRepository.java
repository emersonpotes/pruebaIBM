/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author emers
 */
public interface RecordRepository extends Repository<Record, Integer>{
    List<Record> findAll();
    Record findById(Integer  id);
    Record save(Record r);
    void delete(Record r);
    
    @Query("SELECT t FROM Record t where t.cardid = :id") 
    List<Record> findByCardId(@Param("id") Integer id);

}
