/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author emers
 */
@RestController
public class DBController {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;
    
    @RequestMapping(value="/loaddb", method=RequestMethod.GET)
    public void loadDB() {
        CreateClientsTable();
    }
    
    //@RequestMapping(value="/createClientsTable", method=RequestMethod.GET)
    private void CreateClientsTable(){
        try {
            String sql = "CREATE TABLE IF NOT EXISTS CLIENTS"
                    + "(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50),"
                    + "address VARCHAR(100), city VARCHAR(30), "
                    + "telephone VARCHAR(20), cards VARCHAR(100))";
            jdbcTemplate.execute(sql);
            PutClientsData();
        } catch(DataAccessException e) {
        }
    }
    
    //@RequestMapping(value="/putClientsData", method=RequestMethod.GET)
    private void PutClientsData(){
        List<String[]> clientsV = new ArrayList<String[]>();
        clientsV.add(new String[]{"'Javier'", "'Cra.20 #13-77'",
        "'Medellin'","'3045678921'","'0000-0000-0000-0000'"});
        clientsV.add(new String[]{"'Daniela'", "'Cra.38 #56-07'",
        "'Bogota'","'3018794560'","'1111-1111-1111-1111'"});
        clientsV.add(new String[]{"'Andres'", "'Cra.21 #83-14'",
        "'Medellin'","'3044657901'","'2222-2222-2222-2222'"});
        clientsV.add(new String[]{"'Jorge'", "'Cra.10 #55-66'",
        "'Bogota'","'3018769812'","'3333-3333-3333-3333'"});
        clientsV.add(new String[]{"'Luis'", "'Cra.80 #12-90'",
        "'Cali'","'320134698'","'4444-4444-4444-4444'"});
        clientsV.add(new String[]{"'Fernando'", "'Cra.1 #35-60'",
        "'Medellin'","'3214657809'","'5555-5555-5555-5555'"});
        clientsV.add(new String[]{"'John'", "'Cra.3 #43-87'",
        "'Pereira'","'3204569821'","'6666-6666-6666-6666'"});
        clientsV.add(new String[]{"'Vanesa'", "'Cra.24 #27-33'",
        "'Bogota'","'31656780921'","'7777-7777-7777-7777'"});
        clientsV.add(new String[]{"'Alejandra'", "'Cra.21 #29-90'",
        "'Medellin'","'3107895327'","'8888-8888-8888-8888'"});
        clientsV.add(new String[]{"'Manuel'", "'Cra.33 #37-08'",
        "'Medellin'","'3041298564'","'9999-9999-9999-9999'"});

        String values = "", values_i;
        for(String[] v : clientsV) {
            values_i = "";
            for(String s : v) {
                values_i += s + ",";
            }
            values += "("+values_i.replaceAll("\\s*,\\s*$", "")+"),";
        }
        
        String sql = "INSERT INTO CLIENTS(name, address, city,"
                + "telephone, cards) VALUES " + values.replaceAll("\\s*,\\s*$", "");
        jdbcTemplate.execute(sql);
    }

}
