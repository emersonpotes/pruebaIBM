/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author emers
 */
@CrossOrigin(origins="http://localhost:4200", maxAge=3600)
@RestController
@RequestMapping({"records"})
public class RecordController {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    RecordInterface service;
    
    @GetMapping(path = {"/loaddata"})
    public void loadData() {
        CreateRecordTable();
    }
    
    private void CreateRecordTable(){
        try {
            String sql = "CREATE TABLE IF NOT EXISTS RECORDS"
                    + "(id INT AUTO_INCREMENT PRIMARY KEY, cardid INT, "
                    + "rdate VARCHAR(100), description VARCHAR(100), "
                    + "amount VARCHAR(12))";
            jdbcTemplate.execute(sql);
            PutRecordData();
        } catch(DataAccessException e) {
        }
    }
    
    private void PutRecordData(){
        List<String[]> recordsV = new ArrayList<String[]>();
        recordsV.add(new String[]{"1","1", "'01/01/19'", "'Descripcion Consumo 1'","'$10.000'"});
        recordsV.add(new String[]{"2","1", "'24/01/19'", "'Descripcion Consumo 2'","'$20.000'"});
        recordsV.add(new String[]{"3","2", "'18/02/19'", "'Descripcion Consumo 3'","'$30.000'"});
        recordsV.add(new String[]{"4","2", "'21/02/19'", "'Descripcion Consumo 4'","'$40.000'"});
        recordsV.add(new String[]{"5","3", "'10/03/19'", "'Descripcion Consumo 5'","'$50.000'"});
        recordsV.add(new String[]{"6","3", "'12/03/19'", "'Descripcion Consumo 6'","'$60.000'"});
        recordsV.add(new String[]{"7","4", "'30/04/19'", "'Descripcion Consumo 7'","'$70.000'"});
        recordsV.add(new String[]{"8","4", "'20/04/19'", "'Descripcion Consumo 8'","'$80.000'"});
        recordsV.add(new String[]{"9","5", "'15/05/19'", "'Descripcion Consumo 9'","'$80.000'"});
        recordsV.add(new String[]{"10","5", "'19/05/19'", "'Descripcion Consumo 10'","'$90.000'"});
        recordsV.add(new String[]{"11","6", "'22/06/19'", "'Descripcion Consumo 11'","'$100.000'"});
        recordsV.add(new String[]{"12","6", "'19/06/19'", "'Descripcion Consumo 12'","'$110.000'"});
        recordsV.add(new String[]{"13","7", "'06/06/19'", "'Descripcion Consumo 13'","'$120.000'"});
        recordsV.add(new String[]{"14","7", "'07/07/19'", "'Descripcion Consumo 14'","'$130.000'"});
        recordsV.add(new String[]{"15","8", "'08/07/19'", "'Descripcion Consumo 15'","'$140.000'"});
        recordsV.add(new String[]{"16","8", "'09/08/19'", "'Descripcion Consumo 16'","'$150.000'"});
        recordsV.add(new String[]{"17","9", "'19/08/19'", "'Descripcion Consumo 17'","'$160.000'"});
        recordsV.add(new String[]{"18","9", "'29/09/19'", "'Descripcion Consumo 18'","'$170.000'"});
        recordsV.add(new String[]{"19","10", "'26/09/19'", "'Descripcion Consumo 19'","'$180.000'"});
        recordsV.add(new String[]{"20","10", "'20/10/19'", "'Descripcion Consumo 20'","'$190.000'"});

        recordsV.add(new String[]{"21","11", "'01/01/19'", "'Descripcion Consumo 21'","'$10.000'"});
        recordsV.add(new String[]{"22","11", "'24/01/19'", "'Descripcion Consumo 22'","'$20.000'"});
        recordsV.add(new String[]{"23","12", "'18/02/19'", "'Descripcion Consumo 23'","'$30.000'"});
        recordsV.add(new String[]{"24","12", "'21/02/19'", "'Descripcion Consumo 24'","'$40.000'"});
        recordsV.add(new String[]{"25","13", "'10/03/19'", "'Descripcion Consumo 25'","'$50.000'"});
        recordsV.add(new String[]{"26","13", "'12/03/19'", "'Descripcion Consumo 26'","'$60.000'"});
        recordsV.add(new String[]{"27","14", "'30/04/19'", "'Descripcion Consumo 27'","'$70.000'"});
        recordsV.add(new String[]{"28","14", "'20/04/19'", "'Descripcion Consumo 28'","'$80.000'"});
        recordsV.add(new String[]{"29","15", "'15/05/19'", "'Descripcion Consumo 29'","'$80.000'"});
        recordsV.add(new String[]{"30","15", "'19/05/19'", "'Descripcion Consumo 30'","'$90.000'"});
        recordsV.add(new String[]{"31","16", "'22/06/19'", "'Descripcion Consumo 31'","'$100.000'"});
        recordsV.add(new String[]{"32","16", "'19/06/19'", "'Descripcion Consumo 32'","'$110.000'"});
        recordsV.add(new String[]{"33","17", "'06/06/19'", "'Descripcion Consumo 33'","'$120.000'"});
        recordsV.add(new String[]{"34","17", "'07/07/19'", "'Descripcion Consumo 34'","'$130.000'"});
        recordsV.add(new String[]{"35","18", "'08/07/19'", "'Descripcion Consumo 35'","'$140.000'"});
        recordsV.add(new String[]{"36","18", "'09/08/19'", "'Descripcion Consumo 36'","'$150.000'"});
        recordsV.add(new String[]{"37","19", "'19/08/19'", "'Descripcion Consumo 37'","'$160.000'"});
        recordsV.add(new String[]{"38","19", "'29/09/19'", "'Descripcion Consumo 38'","'$170.000'"});
        recordsV.add(new String[]{"39","20", "'26/09/19'", "'Descripcion Consumo 39'","'$180.000'"});
        recordsV.add(new String[]{"40","20", "'20/10/19'", "'Descripcion Consumo 40'","'$190.000'"});
        
        String values = "", values_i;
        for(String[] v : recordsV) {
            values_i = "";
            for(String s : v) {
                values_i += s + ",";
            }
            values += "("+values_i.replaceAll("\\s*,\\s*$", "")+"),";
        }
        
        String sql = "REPLACE INTO RECORDS(id, cardid, rdate, description, amount)"
                + " VALUES " + values.replaceAll("\\s*,\\s*$", "");
        jdbcTemplate.execute(sql);
    }
    
    @GetMapping
    public List<Record> list() {
        return service.list();
    }
    
    @PostMapping
    public Record add(@RequestBody Record r) {
        return service.add(r);
    }
    
    @GetMapping(path = {"/{id}"})
    public Record listId(@PathVariable("id")int id) {
        return service.listId(id);
    }
    
    @GetMapping(path = {"/getrecords{cardid}"})
    public List<Record> listByCardId(@PathVariable("cardid")int cardid) {
        return service.listByCardId(cardid);
    }

    @PutMapping(path = {"/{id}"})
    public Record edit(@RequestBody Record r, @PathVariable("id")int id) {
        r.setId(id);
        return service.edit(r);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Record delete(@PathVariable("id")int id){
        return service.delete(id);
    }
}
