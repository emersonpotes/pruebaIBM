/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.data.repository.Repository;

/**
 *
 * @author emers
 */
public interface AdviserRepository extends Repository<Adviser, Integer>{
    List<Adviser> findAll();
    Adviser findById(Integer  id);
    Adviser save(Adviser a);
    void delete(Adviser a);
}
