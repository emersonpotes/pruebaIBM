/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author emers
 */
@Service
public class AdviserServiceImpl implements AdviserInterface {
    @Autowired
    private AdviserRepository repository;
    
    @Override
    public List<Adviser> list() {
        return repository.findAll();
    }

    @Override
    public Adviser listId(int id) {
        return repository.findById(id);
    }

    @Override
    public Adviser add(Adviser a) {
        return repository.save(a);
    }

    @Override
    public Adviser edit(Adviser a) {
        return repository.save(a);
    }

    @Override
    public Adviser delete(int id) {
        Adviser a = repository.findById(id);
        if (a != null) repository.delete(a);
        return a;
    }
    
}
