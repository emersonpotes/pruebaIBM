/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author emers
 */
@CrossOrigin(origins="http://localhost:4200", maxAge=3600)
@RestController
@RequestMapping({"cards"})
public class CardController {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    CardInterface service;
    
    @GetMapping(path = {"/loaddata"})
    public void loadData() {
        CreateCardsTable();
    }
    
    private void CreateCardsTable(){
        try {
            String sql = "CREATE TABLE IF NOT EXISTS CARDS"
                    + "(id INT AUTO_INCREMENT PRIMARY KEY, clientid INT, "
                    + "number VARCHAR(100), ccv VARCHAR(4), "
                    + "type VARCHAR(50))";
            jdbcTemplate.execute(sql);
            PutCardsData();
        } catch(DataAccessException e) {
        }
    }
    
    private void PutCardsData(){
        List<String[]> cardsV = new ArrayList<String[]>();
        cardsV.add(new String[]{"1","1", "'0000 0000 0000 0000'", "'1988'","'Master Card'"});
        cardsV.add(new String[]{"2","1", "'1111 1111 1111 1111'", "'2376'","'Visa'"});
        cardsV.add(new String[]{"3","2", "'2222 2222 2222 2222'", "'1068'","'Master Card'"});
        cardsV.add(new String[]{"4","2", "'3333 3333 3333 3333'", "'2465'","'Visa'"});
        cardsV.add(new String[]{"5","3", "'4444 4444 4444 4444'", "'1687'","'Master Card'"});
        cardsV.add(new String[]{"6","3", "'5555 5555 5555 5555'", "'1698'","'Visa'"});
        cardsV.add(new String[]{"7","4", "'6666 6666 6666 6666'", "'8065'","'Master Card'"});
        cardsV.add(new String[]{"8","4", "'7777 7777 7777 7777'", "'3084'","'Visa'"});
        cardsV.add(new String[]{"9","5", "'8888 8888 8888 8888'", "'1280'","'Master Card'"});
        cardsV.add(new String[]{"10","5", "'9999 9999 9999 9999'", "'2485'","'Visa'"});
        cardsV.add(new String[]{"11","6", "'0010 0010 0010 0010'", "'0178'","'Master Card'"});
        cardsV.add(new String[]{"12","6", "'0020 0020 0020 0020'", "'2051'","'Visa'"});
        cardsV.add(new String[]{"13","7", "'0030 0030 0030 0030'", "'2901'","'Master Card'"});
        cardsV.add(new String[]{"14","7", "'0040 0040 0040 0040'", "'1396'","'Visa'"});
        cardsV.add(new String[]{"15","8", "'0050 0050 0050 0050'", "'5052'","'Master Card'"});
        cardsV.add(new String[]{"16","8", "'0060 0060 0060 0060'", "'2793'","'Visa'"});
        cardsV.add(new String[]{"17","9", "'0070 0070 0070 0070'", "'2007'","'Master Card'"});
        cardsV.add(new String[]{"18","9", "'0080 0080 0080 0080'", "'1485'","'Visa'"});
        cardsV.add(new String[]{"19","10", "'0090 0090 0090 0090'", "'1964'","'Master Card'"});
        cardsV.add(new String[]{"20","10", "'1000 1000 1000 1000'", "'1123'","'Visa'"});

        String values = "", values_i;
        for(String[] v : cardsV) {
            values_i = "";
            for(String s : v) {
                values_i += s + ",";
            }
            values += "("+values_i.replaceAll("\\s*,\\s*$", "")+"),";
        }
        
        String sql = "REPLACE INTO CARDS(id, clientid, number, ccv, type)"
                + " VALUES " + values.replaceAll("\\s*,\\s*$", "");
        jdbcTemplate.execute(sql);
    }
    
    @GetMapping
    public List<Card> list() {
        return service.list();
    }
    
    @PostMapping
    public Card add(@RequestBody Card c) {
        return service.add(c);
    }
    
    @GetMapping(path = {"/{id}"})
    public Card listId(@PathVariable("id")int id) {
        return service.listId(id);
    }
    
    @GetMapping(path = {"/getcards{clientid}"})
    public List<Card> listByClientId(@PathVariable("clientid")int clientid) {
        return service.listByClientId(clientid);
    }

    @PutMapping(path = {"/{id}"})
    public Card edit(@RequestBody Card c, @PathVariable("id")int id) {
        c.setId(id);
        return service.edit(c);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Card delete(@PathVariable("id")int id){
        return service.delete(id);
    }
}
