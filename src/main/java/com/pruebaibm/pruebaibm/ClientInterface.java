/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;

/**
 *
 * @author emers
 */
public interface ClientInterface {
    List<Client>list();
    Client listId(int id);
    Client add(Client c);
    Client edit(Client c);
    Client delete(int id);
}
