/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author emers
 */
@Service
public class CardServiceImpl implements CardInterface {
    @Autowired
    private CardRepository repository;
    
    @Override
    public List<Card> list() {
        return repository.findAll();
    }

    @Override
    public Card listId(int id) {
        return repository.findById(id);
    }
    @Override
    public List<Card> listByClientId(int clientid) {
        return repository.findByClientId(clientid);
    }

    @Override
    public Card add(Card c) {
        return repository.save(c);
    }

    @Override
    public Card edit(Card c) {
        return repository.save(c);
    }

    @Override
    public Card delete(int id) {
        Card c = repository.findById(id);
        if (c != null) repository.delete(c);
        return c;
    }
    
}
