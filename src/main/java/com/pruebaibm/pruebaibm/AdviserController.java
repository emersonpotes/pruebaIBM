/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author emers
 */
@CrossOrigin(origins="http://localhost:4200", maxAge=3600)
@RestController
@RequestMapping({"advisors"})
public class AdviserController {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    AdviserInterface service;
    
    @GetMapping(path = {"/loaddata"})
    public void loadData() {
        CreateAdviserTable();
    }
    
    private void CreateAdviserTable(){
        try {
            String sql = "CREATE TABLE IF NOT EXISTS ADVISORS"
                    + "(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50), "
                    + "speciality VARCHAR(50))";
            jdbcTemplate.execute(sql);
            PutAdviserData();
        } catch(DataAccessException e) {
        }
    }
    
    private void PutAdviserData(){
        List<String[]> advisersV = new ArrayList<String[]>();
        advisersV.add(new String[]{"1", "'Fernando'", "'Créditos'"});
        advisersV.add(new String[]{"2","'Raul'", "'Créditos'"});
        advisersV.add(new String[]{"3","'Javier'", "'Ahorros'"});
        advisersV.add(new String[]{"4","'Jose'", "'Créditos'"});
        advisersV.add(new String[]{"5","'Claudia'", "'Ahorros'"});
        advisersV.add(new String[]{"6","'Jessica'", "'Ahorros'"});
        advisersV.add(new String[]{"7","'Diana'", "'Créditos'"});
        advisersV.add(new String[]{"8","'Carolina'", "'Ahorros'"});
        advisersV.add(new String[]{"9","'Bernardo'", "'Créditos'"});
        advisersV.add(new String[]{"10","'Mauricio'", "'Ahorros'"});

        String values = "", values_i;
        for(String[] v : advisersV) {
            values_i = "";
            for(String s : v) {
                values_i += s + ",";
            }
            values += "("+values_i.replaceAll("\\s*,\\s*$", "")+"),";
        }
        
        String sql = "REPLACE INTO ADVISORS(id, name, speciality)"
                + " VALUES " + values.replaceAll("\\s*,\\s*$", "");
        jdbcTemplate.execute(sql);
    }
    
    @GetMapping
    public List<Adviser> list() {
        return service.list();
    }
    
    @PostMapping
    public Adviser add(@RequestBody Adviser a) {
        return service.add(a);
    }
    
    @GetMapping(path = {"/{id}"})
    public Adviser listId(@PathVariable("id")int id) {
        return service.listId(id);
    }

    @PutMapping(path = {"/{id}"})
    public Adviser edit(@RequestBody Adviser a, @PathVariable("id")int id) {
        a.setId(id);
        return service.edit(a);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Adviser delete(@PathVariable("id")int id){
        return service.delete(id);
    }
}
