/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;

/**
 *
 * @author emers
 */
public interface RecordInterface {
    List<Record>list();
    Record listId(int id);
    Record add(Record r);
    Record edit(Record r);
    Record delete(int id);
    List<Record> listByCardId(int cardid);
}
