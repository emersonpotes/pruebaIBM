/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;

/**
 *
 * @author emers
 */
public interface CardInterface {
    List<Card>list();
    Card listId(int id);
    Card add(Card c);
    Card edit(Card c);
    Card delete(int id);
    List<Card> listByClientId(int clientid);
}
