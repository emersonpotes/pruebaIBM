/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author emers
 */
public interface CardRepository extends Repository<Card, Integer>{
    List<Card> findAll();
    Card findById(Integer  id);
    Card save(Card c);
    void delete(Card c);
    
    @Query("SELECT t FROM Card t where t.clientid = :id") 
    List<Card> findByClientId(@Param("id") Integer id);
}
