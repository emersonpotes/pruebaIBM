/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.data.repository.Repository;

/**
 *
 * @author emers
 */
public interface ClientRepository extends Repository<Client, Integer>{
    List<Client> findAll();
    Client findById(Integer  id);
    Client save(Client c);
    void delete(Client c);
}
