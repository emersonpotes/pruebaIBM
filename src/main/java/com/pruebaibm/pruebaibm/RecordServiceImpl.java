/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author emers
 */
@Service
public class RecordServiceImpl implements RecordInterface {
    @Autowired
    private RecordRepository repository;
    
    @Override
    public List<Record> list() {
        return repository.findAll();
    }

    @Override
    public Record listId(int id) {
        return repository.findById(id);
    }

    @Override
    public List<Record> listByCardId(int recordid) {
        return repository.findByCardId(recordid);
    }

    @Override
    public Record add(Record r) {
        return repository.save(r);
    }

    @Override
    public Record edit(Record r) {
        return repository.save(r);
    }

    @Override
    public Record delete(int id) {
        Record r = repository.findById(id);
        if (r != null) repository.delete(r);
        return r;
    }
    
}
