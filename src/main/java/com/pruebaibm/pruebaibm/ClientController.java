/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

/**
 *
 * @author emers
 */
@CrossOrigin(origins="http://localhost:4200", maxAge=3600)
@RestController
@RequestMapping({"clients"})
public class ClientController {
    
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    ClientInterface service;
    
    @GetMapping(path = {"/loaddata"})
    public void loadData() {
        CreateClientsTable();
    }
    
    private void CreateClientsTable(){
        try {
            String sql = "CREATE TABLE IF NOT EXISTS CLIENTS"
                    + "(id INT AUTO_INCREMENT PRIMARY KEY, name VARCHAR(50),"
                    + "address VARCHAR(100), city VARCHAR(30), "
                    + "telephone VARCHAR(20), cards VARCHAR(100))";
            jdbcTemplate.execute(sql);
            PutClientsData();
        } catch(DataAccessException e) {
        }
    }
    
    private void PutClientsData(){
        List<String[]> clientsV = new ArrayList<String[]>();
        clientsV.add(new String[]{"1","'Javier'", "'Cra.20 #13-77'",
        "'Medellin'","'3045678921'","'0000-0000-0000-0000'"});
        clientsV.add(new String[]{"2","'Daniela'", "'Cra.38 #56-07'",
        "'Bogota'","'3018794560'","'1111-1111-1111-1111'"});
        clientsV.add(new String[]{"3","'Andres'", "'Cra.21 #83-14'",
        "'Medellin'","'3044657901'","'2222-2222-2222-2222'"});
        clientsV.add(new String[]{"4","'Jorge'", "'Cra.10 #55-66'",
        "'Bogota'","'3018769812'","'3333-3333-3333-3333'"});
        clientsV.add(new String[]{"5","'Luis'", "'Cra.80 #12-90'",
        "'Cali'","'320134698'","'4444-4444-4444-4444'"});
        clientsV.add(new String[]{"6","'Fernando'", "'Cra.1 #35-60'",
        "'Medellin'","'3214657809'","'5555-5555-5555-5555'"});
        clientsV.add(new String[]{"7","'John'", "'Cra.3 #43-87'",
        "'Pereira'","'3204569821'","'6666-6666-6666-6666'"});
        clientsV.add(new String[]{"8","'Vanesa'", "'Cra.24 #27-33'",
        "'Bogota'","'31656780921'","'7777-7777-7777-7777'"});
        clientsV.add(new String[]{"9","'Alejandra'", "'Cra.21 #29-90'",
        "'Medellin'","'3107895327'","'8888-8888-8888-8888'"});
        clientsV.add(new String[]{"10","'Manuel'", "'Cra.33 #37-08'",
        "'Medellin'","'3041298564'","'9999-9999-9999-9999'"});

        String values = "", values_i;
        for(String[] v : clientsV) {
            values_i = "";
            for(String s : v) {
                values_i += s + ",";
            }
            values += "("+values_i.replaceAll("\\s*,\\s*$", "")+"),";
        }
        
        String sql = "REPLACE INTO CLIENTS(id, name, address, city,"
                + "telephone, cards) VALUES " + values.replaceAll("\\s*,\\s*$", "");
        jdbcTemplate.execute(sql);
    }
    
    @GetMapping
    public List<Client> list() {
        return service.list();
    }
    
    @PostMapping
    public Client add(@RequestBody Client c) {
        return service.add(c);
    }
    
    @GetMapping(path = {"/{id}"})
    public Client listId(@PathVariable("id")int id) {
        return service.listId(id);
    }
    
    @PutMapping(path = {"/{id}"})
    public Client edit(@RequestBody Client c, @PathVariable("id")int id) {
        c.setId(id);
        return service.edit(c);
    }
    
    @DeleteMapping(path = {"/{id}"})
    public Client delete(@PathVariable("id")int id){
        return service.delete(id);
    }
}
