/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pruebaibm.pruebaibm;

import java.util.List;

/**
 *
 * @author emers
 */
public interface AdviserInterface {
    List<Adviser>list();
    Adviser listId(int id);
    Adviser add(Adviser a);
    Adviser edit(Adviser a);
    Adviser delete(int id);
}
